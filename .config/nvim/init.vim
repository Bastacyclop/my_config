" augroup textfiles
"   autocmd!
"  autocmd filetype markdown :setlocal spell spelllang=en | syntax clear
" augroup end

set showmode " display current mode
set ruler " display position
set cursorline " highlight current line

" display absolute/relative line numbers
set number
set relativenumber
autocmd InsertEnter * set norelativenumber
autocmd InsertLeave * set relativenumber

" preferred text width
set textwidth=80
set colorcolumn=+0 " highlight
set wrap

set showmatch " show bracket match
set showcmd " show the command you are typing

" indentation
set tabstop=2
set expandtab " hitting tab will produce spaces
set shiftwidth=2
set softtabstop=2
set smartindent
set cindent

" case-insensitive search
set ignorecase
" except when search contains uppercase
set smartcase

" automatically fold indented stuff
set foldmethod=indent

set termguicolors
colorscheme NeoSolarized
set background=light

""""

" true tabulations for Makefile
autocmd FileType Makefile set noexpandtab

