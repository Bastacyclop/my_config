# My Config

My config on Manjaro Linux with XFCE.

# Usage

```sh
git clone git://gitlab.com/Bastacyclop/my_config
# import a dotfile in the config git
$CONFIG_DIR/import.sh .dotfile
# export a dotfile from the config git
$CONFIG_DIR/export.sh .dotfile
```

# Favorite programs

- **terminal**: termite, neovim (NeoSolarized), tmux?
- **package**: aurman
- **files**: exa, ranger, thunar
- **remote**: dropbox, syncthing, sshfs
- **code**: vscode, emacs, neovim
- **devel**: base-devel, git
- **build**: ninja
- **compile**: gcc, clang
- **debug & profile**: gdb, valgrind, apitrace
- **communicate**: discord, signal
- **browse**: firefox
- **office**: libreoffice
- **read media**: viewnior, mupdf, vlc
- **edit media**: inkscape, krita
- Manjaro XFCE built-ins!

## Themes

- solarized light
- vertex \[maia\] (gtk)

## Fonts

fira, noto
