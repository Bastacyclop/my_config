(use-package company-coq
  :requires company
  :defer t
  :config
  (add-hook 'coq-mode-hook #'company-code-mode))

(use-package proof-site
  :defer t
  :load-path "local/proof-general/generic/"
  :mode ("\\.v\\'" . coq-mode)
  :init
  (setq proof-three-window-mode-policy 'hybrid
        proof-script-fly-past-comments t
        proof-splash-seen t)
  :config
  (defun proof-general-setup ()
    (general-define-key :prefix major-leader
                        :non-normal-prefix alt-major-leader
                        :states '(normal insert emacs)
                        :keymaps 'local
                        "p" 'coq-Print
                        ;; coq-insert-match
                        "." 'proof-goto-point
                        "p" 'proof-backward-command
                        "n" 'proof-forward-command
                        "k" 'proof-interrupt-process
                        ;; 'proof-goto-command-end
                        ;; 'proof-goto-command-start
                        "b" 'proof-process-buffer
                        ;; coq-Search
                        "s" '(proof-find-theorems :which-key "search")
                        ;; coq-Check
                        "c" '(proof-query-identifier :which-key "check")))
  (add-hook 'coq-mode-hook #'proof-general-setup)
  (add-hook 'coq-mode-hook 'smartparens-mode))
