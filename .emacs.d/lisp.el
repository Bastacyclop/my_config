(use-package auto-compile
  :defer t)
(use-package debug
  :defer t)
(use-package edebug
  :defer t)
(use-package ielm
  :defer t)
(use-package eldoc
  :defer t
  :diminish "")

;; not working and slowing startup ..
;; (use-package emacs-lisp
;;   :ensure t
;;   :diminish (emacs-lisp-mode . "ELisp"))

;; common-lisp-snippets
;; parinfer
;; macrostep

(use-package slime ; common lisp
  :defer t
  :commands slime-mode
  :config
  (slime-setup))

;; slime-company

(use-package evil-lispy ; lisp-like edition
  :defer t
  :config
  (add-hook 'emacs-lisp-mode-hook #'evil-lispy-mode)
  (add-hook 'slime-mode #'evil-lispy-mode)
  (add-hook 'clojure-mode-hook #'evil-lispy-mode))
