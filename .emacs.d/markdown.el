(use-package markdown-mode
  :defer t
  :mode (("\\.md\\'" . markdown-mode)))

;; markdown-toc
