;; (use-package cider-eval-sexp-fu)
;; (use-package clojure-snippets)
;; (use-package clj-refactor)
;; subword-mode
;; paredit
;; rainbow-delimiters

(defun clojure-local-setup ()
  (general-define-key :prefix major-leader
                      :non-normal-prefix alt-major-leader
                      :states '(normal insert visual emacs)
                      :keymaps 'local
                      "h" '(:ignore t :which-key "help")
                      "ha" 'cider-apropos
                      "hd" 'cider-doc
                      "hg" 'cider-grimoire
                      "hn" 'cider-browse-ns
                      "e" '(:ignore t :which-key "eval")
                      "eb" 'cider-eval-buffer
                      "ee" 'cider-eval-last-sexp
                      "ef" 'cider-eval-defun-at-point
                      "em" 'cider-macroexpand-1
                      "eM" 'cider-macroexpand-all
                      "er" 'cider-eval-region
                      "ew" 'cider-eval-last-sexp-and-replace
                      "=" 'cider-format-buffer
                      "'" 'cider-jack-in
                      "\"" 'cider-jack-in-clojurescript
                      "d" '(:ignore t :which-key "debug")
                      "db" 'cider-debug-defun-at-point
                      "dv" 'cider-inspect
                      "r" '(:ignore t :which-key "refactor")
                      "rc" '(:ignore t :which-key "collection")
                      "rc{" 'clojure-convert-collection-to-map
                      "rc(" 'clojure-convert-collection-to-list
                      "rc'" 'clojure-convert-collection-to-quoted-list
                      "rc#" 'clojure-convert-collection-to-set
                      "rc[" 'clojure-convert-collection-to-vector))

;; (setq package-check-signature nil)

(use-package cider
  :ensure t ; TODO: defer ?
  :pin melpa-stable
  :init
  (setq cider-stacktrace-default-filters '(tooling dup)
        cider-repl-pop-to-buffer-on-connect nil
        cider-prompt-save-file-on-load nil
        cider-repl-use-clojure-font-lock t)
  :config
  (progn
    (evil-set-initial-state 'cider-stacktrace-mdoe 'motion)
    (evil-set-initial-state 'cider-popup-buffer-mode 'motion)
    (setq cider-prompt-for-symbol nil)))

(use-package clojure-mode
  :ensure t ; TODO: defer ?
  :mode ("\\.clj\\'" . clojure-mode)
  :pin melpa-stable
  :init
  (add-hook 'clojure-mode-hook #'cider-mode)
  (add-hook 'clojure-mode-hook #'smartparens-mode)
  (add-hook 'clojure-mode-hook #'agressive-indent-mode)
  (add-hook 'clojure-mode-hook #'clojure-local-setup)
  :config
  (setq clojure-indent-style :align-arguments)
  (put-clojure-indent 'match 1))
