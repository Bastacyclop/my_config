;; requires opam and
;; opam install tuareg merlin

(defun ocaml-local-setup ()
  (general-define-key :prefix major-leader
                      :non-normal-prefix alt-major-leader
                      :states '(normal insert visual emacs)
                      :keymaps 'local
                      "e" 'merlin-error-next
                      "E" 'merlin-error-prev
                      ;; merlin-destruct
                      "p" 'merlin-goto-project-file
                      "j" 'merlin-jump
                      "l" 'merlin-locate
                      ;; merlin-phrase-next/prev
                      ;; merlin-switch-to-ml[i]
                      "d" 'merlin-document
                      "o" 'merlin-occurences
                      "t" 'merlin-type-enclosing))

(use-package merlin
  ;; :requires flycheck-ocaml
  :ensure t) ; TODO: defer
  ;; :init
  ;; (flycheck-ocaml-setup)
  ;; :config
  ;; (add-to-list 'company-backends 'merlin-company-backend))

;; (use-package ocp-indent
;;   :ensure t
;;   :init
;;   (add-hook 'tuareg-mode-hook 'ocp-indent-caml-mode-setup))

;; (load "/home/basta/.opam/ocaml-system/share/emacs/site-lisp/tuareg-site-file")

(use-package tuareg
  :defer t
  :mode ("\\.ml[ily]?$" . tuareg-mode)
  :init
  (setq opam-share "~/.opam/ocaml-system/share"
        opam-load-path (concat opam-share "/emacs/site-lisp"))
  (add-to-list 'load-path opam-load-path)
  ;; Make OCaml-generated files invisible to filename completion
  (dolist (ext '(".cmo" ".cmx" ".cma" ".cmxa" ".cmi" ".cmxs" ".cmt" ".cmti" ".annot"))
    (add-to-list 'completion-ignored-extensions ext))
  :config
  ;; (add-hook 'tuareg-mode-hook 'flycheck-mode)
  (add-hook 'tuareg-mode-hook 'smartparens-mode)
  (add-hook 'tuareg-mode-hook 'merlin-mode)
  (add-hook 'tuareg-mode-hook 'company-mode)
  ;; (add-hook 'tuareg-mode-hook 'utop-minor-mode)
  (add-hook 'tuareg-mode-hook 'ocaml-local-setup))

;; (use-package utop
;;   :ensure t ; TODO: defer
;;   :init
;;   (setq utop-command "opam config exec -- utop -emacs"))
