;; -*- emacs-lisp -*-

;; don't GC during startup to save time
(setq gc-cons-threshold most-positive-fixnum)

(setq package-enable-at-startup nil)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents) ; update packages archive
  (package-install 'use-package))
(require 'use-package)

(use-package server
  :config
  (unless (server-running-p) (server-start)))

;; Sane defaults

(setq-default use-package-verbose nil
              delete-old-versions t ; delete excess backups
              version-control t
              vc-make-backup-files t ; backup even with version control
              backup-directory-alist '(("." . "~/.emacs.d/backups"))
              vc-follow-symlins t
              auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list" t))
              inhibit-startup-screen t
              create-lockfiles nil
              ring-bell-function 'ignore
              coding-system-for-read 'utf-8
              coding-system-for-write 'utf-8
              sentence-en-double-space nil
              ;; default-fill-column 80
              initial-scratch-message "Welcome in Emacs"
              ;; save-interprogram-paste-before-kill t
              ;; select-enable-clipboard nil
              x-select-enable-clipboard-manager nil
              ;; help-window-select t
              indent-tabs-mode nil
              tab-width 4)

(prefer-coding-system 'utf-8)
(show-paren-mode)
(line-number-mode)
(column-number-mode)
(save-place-mode) ; save cursor position
;; (delete-selection-mode 1) ; replace text with type
(setq initial-major-mode 'fundamental-mode)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

;; Display

(when window-system
  (tooltip-mode -1)
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (blink-cursor-mode -1)
  (add-to-list 'default-frame-alist '(font . "Fira Code 11"))
  (set-face-attribute 'default nil :font "Fira Code 11"))

;;(use-package emacs-dashboard
;;         :ensure t
;;         :config
;;         (dashboard-setup-startup-hook)
;;         (dashboard-items '((recents . 5)
;;                (bookmarks . 5)
;;                (projects . 5)
;;                      (registers . 5))))

;; Packages

(use-package async
  :ensure t
  :config
  (async-bytecomp-package-mode 1))

(use-package evil
  :ensure t
  :init
  (setq evil-want-abbrev-expand-on-insert-exit nil)
  :config
  (evil-mode 1))

(use-package general ; keybindings
  :ensure t
  :config
  ;; bind a key globally in normal state; keymaps must be quoted
  ;; (setq general-default-keymaps 'evil-normal-state-map)
  (setq main-leader "SPC")
  (setq alt-main-leader "C-SPC")
  (setq major-leader ",")
  (setq alt-major-leader "C-,")
  (general-define-key :prefix main-leader
                      :non-normal-prefix alt-main-leader
                      :states '(normal visual insert emacs)
                      "q" '(:ignore t :which-key "quit")
                      "qq" 'save-buffers-kill-emacs
                      "s" 'swiper
                      "x" 'counsel-M-x
                      "f" '(:ignore t :which-key "files")
                      "ff" 'counsel-find-file
                      "fL" 'counsel-locate
                      "fr" 'counsel-recentf
                      "b" '(:ignore t :which-key "buffers")
                      "bs" 'ivy-switch-buffer
                      "bn" 'next-buffer
                      "bp" 'previous-buffer
                      "bk" 'kill-buffer
                      ;; save buffer ? :w
                      ;; save all ?
                      "c" '(:ignore t :which-key "compile")
                      "e" '(:ignore t :which-key "errors")
                      "ec" 'flycheck-clear
                      "eh" 'flycheck-describe-checker
                      "ee" 'flycheck-explain-error-at-point
                      "es" 'flycheck-select-checker
                      "ev" 'flycheck-verify-setup
                      "en" 'flycheck-next-error
                      "ep" 'flycheck-previous-error
                      "m" '(:ignore t :which-key "move")
                      "ms" 'forward-sexp
                      "mS" 'backward-sexp
                      "mf" 'beginning-of-defun
                      "mF" 'end-of-defun
                      "d" '(:ignore t :which-key "delete")
                      "ds" 'kill-sexp
                      ;; "t" '(:ignore t :which-key "toggles")
                      "t" '(:ignore t :which-key "tag")
                      "ts" 'mark-sexp
                      "tf" 'mark-defun
                      "w" '(:ignore t :which-key "windows")
                      "w:" 'split-window-right
                      "w!" 'split-window-below
                      "w=" 'balance-windows
                      "wq" 'quit-window
                      "wk" 'kill-buffer-and-window
                      "w-SPC" 'other-window
                      "w <left>" 'windmove-left
                      "w <right>" 'windmove-right
                      "w <up>" 'windmove-up
                      "w <down>" 'windmove-down
                      "?" 'counsel-descbinds
                      "h" '(:ignore t :which-key "help")
                      "hs" 'blank-mode ; TODO
                      "hd" '(:ignore t :which-key "describe")
                      "hdb" 'describe-bindings
                      "hdc" 'describe-char
                      "hdk" 'describe-key
                      "hdp" 'describe-package
                      "hdt" 'describe-theme
                      "hdv" 'counsel-describe-variable
                      "hdf" 'counsel-describe-function
                      "hdm" 'counsel-describe-mode
                      "i" '(:ignore t :which-key "insert")
                      "iu" 'counsel-insert-unicode
                      "p" '(:ignote t :which-key "projects")
                      "pa" 'projectile-project-info
                      "ps" 'projectile-multi-occur
                      "pc" 'projectile-compile-project
                      "pr" 'projectile-run-project
                      "pt" 'projectile-test-project
                      "pf" 'projectile-find-file
                      "po" 'projectile-switch-project
                      "g" '(:ignore t :which-key "git")
                      ))

;; flycheck-pos-tip, flycheck-popup-tip?

(use-package which-key
  :ensure t
  :diminish ""
  :init ; before package load
  (which-key-mode)
  ;;         :config ; after package load
  ;;         (which-key-setup-side-window-right-bottom)
  ;;         (setq which-key-sort-order 'which-key-key-order-alpha
  ;;           which-key-side-window-max-width 0.33
  ;;           which-key-idle-delay 0.05)
  )

(use-package ivy ; completion mechanisms
  :ensure t
  :init
  (ido-mode 0)
  (ivy-mode 1)
  :config
  (setq ivy-use-virtual-buffers t
        enable-recursive-minibuffers t))

(use-package counsel ; ivy-enhanced commands
  :ensure t
  :diminish ""
  :init
  (counsel-mode 1))

(use-package swiper ; ivy-enhanced search
  :ensure t)

;; ivy-hydra, counsel-gtags

;; anzu, search numbers
;; avy, jump to text with char decision tree
;; floobits?

(use-package aggressive-indent ; keeps things indented correctly
  :defer t
  :diminish ""
  :commands aggressive-indent-mode)

(use-package blank-mode ; show blanks
  :ensure t
  :commands blank-mode)

;; TODO
(use-package company ; completion framework
  :ensure t
  :commands global-company-mode
  :init
  (add-hook 'after-init-hook #'global-company-mode)
  (setq
   company-idle-delay 0.2
   company-selection-wrap-around t
   company-minimum-prefix-length 2
   company-require-match nil
   company-show-numbers t)
  :config
  (global-company-mode)
  (use-package company-statistics
    :config
    (company-statistics-mode))
  (bind-keys :map company-active-map
             ("TAB" . company-complete))
  (setq company-backends
        '((company-clang
           ;; company-semantic
           ;; company-files
           ;; company-gtags
           ;; company-etags
           ;; company-keywords
           )))
  ;; (setq hippie-expand-try-functions-list ..)
  )

;; auto-complete, company-quickhelp, company-statistics, fuzzy, hippie-exp, yasnippet

(use-package smartparens ; TODO: use more!
  :ensure t)

;; dired? ranger?
;; dumb-jump, jump to definition

;; (use-package dash/dash-functional) ; lists
;; (use-package s) ; string manipulation

(use-package origami ; text folding
  :ensure t
  :config
  (global-origami-mode))

(use-package esup ; emacs startup profiler
  :ensure t
  :commands esup)

(use-package flx :ensure t) ; fuzzy matcher

(use-package flycheck ; syntax checking
  :ensure t
  :commands flycheck-mode
  :diminish flycheck-mode
  :config
  (setq flycheck-highlighting-mode 'symbols))

;; ggtags?

(use-package git-gutter ; git diff in left margin
  :ensure t
  :diminish ""
  :commands (global-git-gutter-mode)
  :init
  (global-git-gutter-mode 1)
  (setq git-gutter:modified-sign "|")
  (setq git-gutter:added-sign "+")
  (setq git-gutter:deleted-sign "-")
  :config
  (setq git-gutter:update-interval 20)
  (git-gutter:linum-setup))

(use-package hl-line ; highlight current line
  :init
  (global-hl-line-mode 1))

(use-package hungry-delete ; delete while whitespace
  :ensure t
  :diminish ""
  :config
  (global-hungry-delete-mode))

;; why is electric-indent activated?
(setq eletric-indent-mode nil)

;; TODO: hydra, ibuffer?, ido?

(use-package iedit ; edit multiple regions simultaneously
  :ensure t
  ;; TODO
  )

;; ispell?, isend-mode?

(use-package lorem-ipsum
  :ensure t
  :commands (lorem-ipsum-insert-list
             lorem-ipsum-insert-sentences
             lorem-ipsum-insert-paragraphs))

;; magit?

(use-package move-text
  :ensure t
  :commands (move-text-down
             move-text-up))

;; multiple-cursors?
;; openwith, open files with external applications
;; paradox, package menu

(use-package pdf-tools
  :defer t
  :mode ("\\.pdf\\'" . pdf-view-mode)
  :config
  (pdf-tools-install)
  (setq pdf-tools-enabled-modes
        '(pdf-links-minor-mode
          ;; pdf-isearch-minor-mode
          ;; pdf-misc-minor-mode
          ;; pdf-outline-minor-mode
          ;; pdf-sync-minor-mode
          ;; ..
          ))
  (require 'pdf-occur)
  (require 'pdf-links)
  (require 'pdf-outline)
  (require 'pdf-sync)
  (pdf-tools-install)
  (setq pdf-view-continuous t))

;; persp-mode?, perspeen?, pretty-mode?
;; outline?

(use-package projectile
  :ensure t
  :config
  (projectile-global-mode))

(use-package counsel-projectile
  :ensure t
  :config
  (counsel-projectile-mode))

(use-package rainbow-delimiters ; TODO
  :ensure t
  :commands rainbow-delimiters-mode
  :init
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

;; ranger?, recentf?

(use-package rg ; ripgrep, rust searcher
  :ensure t
  :commands rg)

;; selected?, evil-numbers?

(setq custom-theme-load-path '("~/.emacs.d/themes/solarized"))
(load-theme 'solarized t)

(use-package smooth-scrolling
  :ensure t
  :config
  (smooth-scrolling-mode)
  (setq smooth-scroll-margin 5))

;; subword?
;; tile?, visual-fill-column?, web-mode?
;; eval-sexp-fu, expand-region, hexl, link-hint, uuidgen, ws-butler?

(use-package undo-tree
  :diminish ""
  :config
  (global-undo-tree-mode))

(dolist (name '("lisp"
                "makefile"
                "markdown"
                "coq"
                "clojure"
                "ocaml"))
  (load-file (concat user-emacs-directory name ".el")))

;; move custom set variables out of here
(setq custom-file (concat user-emacs-directory "custom-set-variables.el"))
(load custom-file 'noerror)

;; best value hopefuly
(setq gc-cons-threshold 4000000)
