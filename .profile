export EDITOR=nvim
export BROWSER=firefox
export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

for path in "$HOME/.cargo/bin" "$HOME/.local/bin"; do
    if [ -d $path ]; then
        PATH="$path:$PATH"
    fi
done

DOTNET_ROOT=/opt/dotnet
if [ -d $DOTNET_ROOT ]; then
    export DOTNET_ROOT
fi
