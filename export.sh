#!/bin/sh

config_dir=`dirname $0`
config_dir=`readlink -fn $config_dir`
target_dir=`dirname $HOME/$1`
ln -si --target-directory=$target_dir $config_dir/$1
