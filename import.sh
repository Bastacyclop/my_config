#!/bin/sh

config_dir=`dirname $0`
target=$config_dir/$1
target_dir=`dirname $target`
mkdir -p $target_dir
mv -i $HOME/$1 $target
$config_dir/export.sh $1
